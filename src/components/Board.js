import React from "react";
import Cell from "./Cell";

const boardStyle = {
    display: "grid",
    width: "600px",
    height: "calc(100%)",
    grid: "auto-flow dense / 1fr 1fr 1fr",
    gridAutoRows: "auto"
};

class Board extends React.Component {
    render() {
        return <div style={boardStyle}>{this.props.cells.map((c, index) => <Cell content={c}
                                                                                 onClick={() => this.props.handleCellClick(index)}/>)}</div>
    }
}

export default Board;
