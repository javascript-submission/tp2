import React from "react";

const cellStyle = {
    display: "block",
    backgroundColor: "white",
    width: "200px",
    height: "200px",
    border: "1px solid #333",
    outline: "none",
    textAlign: "center",
    lineHeight: "200px",
    cursor: "pointer"
};

const overStyle = {
    display: "block",
    backgroundColor: "lightgray",
    width: "200px",
    height: "200px",
    border: "1px solid #333",
    outline: "none",
    textAlign: "center",
    lineHeight: "200px",
    cursor: "pointer"
};

class Cell extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            MouseOver: false
        };
    }

    handleMouseOver() {
        this.setState({MouseOver: true})
    }

    handleMouseOut() {
        this.setState({MouseOver: false})
    }

    render() {
        return <div style={this.state.MouseOver ? overStyle : cellStyle} onMouseOver={() => this.handleMouseOver()}
                    onMouseOut={() => this.handleMouseOut()} onClick={() => this.props.onClick()}>{this.props.content}</div>
    }
}

export default Cell;
