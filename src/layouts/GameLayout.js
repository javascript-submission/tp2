import React from "react";
import Board from "../components/Board";
import GameInfo from "../components/GameInfo";

const gameLayoutStyle = {
    width: 650,
    height: `calc(90%)`,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: "auto"
};

class GameLayout extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cells: Array(9).fill(null),
            currentPlayer: "player 1",
            nextSymbol: "O"
        };
    }

    handleCellClick(index) {
        if (index != null && index < this.state.cells.length && this.state.cells[index] == null) {
            this.state.cells[index] = this.state.nextSymbol;
            if (this.state.currentPlayer === "player 1")
                this.setState({currentPlayer: "player 2", nextSymbol: "X"});
            else
                this.setState({currentPlayer: "player 1", nextSymbol: "O"});
        }
    }
    static checkWin(cells) {
        let s = null;
        for (let offset = 0; offset < 3; offset++) {

            //Column checking
            s = cells[offset];
            if (s !== null)
                for (let i = 1; i < 3; i++) {
                    if (cells[i * 3 + offset] !== s)
                        break;
                    if (i === 2)
                        return true;
                }

            //Line checking
            s = cells[offset * 3];
            if (s !== null)
                for (let i = 1; i < 3; i++) {
                    if (cells[offset * 3 + i] !== s)
                        break;
                    if (i === 2)
                        return true;
                }
        }

        //Diag TL-BR checking
        s = cells[0];
        if (s !== null)
            for (let i = 1; i < 3; i++) {
                if (cells[i * 4] !== s)
                    break;
                if (i === 2)
                    return true;
            }

        //Diag TR-BL checking
        s = cells[2];
        if (s !== null)
            for (let i = 1; i < 3; i++) {
                if (cells[(i + 1) * 2] !== s)
                    break;
                if (i === 2)
                    return true;
            }

        return false;
    }

    static checkDraw(cells) {
        return cells.find((e) => (e === null)) !== null
    }

    // getDerivedStateFromProps is called before every render,
    // use it to infer new state values from props or state changes.
    static getDerivedStateFromProps(props, state) {
        if(GameLayout.checkWin(state.cells) || GameLayout.checkDraw(state.cells)) {
            if(GameLayout.checkWin(state.cells)) {
                console.log(state.currentPlayer + " won!");

            }
            else
                console.log("it's a draw :(");
            state.cells = Array(9).fill(null);
        }
        return state;
    }

    render() {
        return (
            <div
                style={gameLayoutStyle}
            >
                <GameInfo currentPlayer={this.state.currentPlayer}/>
                <Board cells={this.state.cells} handleCellClick={(index) => this.handleCellClick(index)}/>
            </div>
        );
    }
}

export default GameLayout;
